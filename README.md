# Quadruped

Dépendances à installer:

    pip install numpy scipy pybullet jupyter matplotlib

Ensuite:

      python quadruped.py -m mode [-x x-cible -y y-cible -t t-cible]

Travail réalisé :

leg_ik :   
          1 solution correspond au cas où la patte est tendue ce qui décrit un quart de sphère. Au delà de cette espace il n'existe pas de solution possible puisqu'on a dépassé la portée de la patte. L'infinité de solution (singularité) correspond aux points de l'axe du premier moteur. Le cas où on a deux solution correspond au cas de symétrie ( coude vers le haut et vers le bas )
          Pour me contenir au cas où il n'y à qu'une solution, j'ai borné les valeurs de x,y,z.

robot_ik :
          La fonction robot_ik prend en entrée un angle de rotation (t) et une position souhaitée du corps du robot (x,y,z).
          Ces différents paramètres sont données dans le repère du corps du robot.
          Cette fonction calcule le changement de base (du corps du robot vers chaques pattes) et applique les nouvelles coordonnées au différents pattes.

walk :
          Walk prend en paramètre x_speed, y_speed et t_speed, respectivement en m/s, m/s et rad/s et va devoir calculer à chaques instant t des positions ces différentes positions doivent permettre de faire marcher le robot.
          J'ai effectué plusieurs tests au niveau de la marche car il existe plusieurs façon de faire avancée le robot.
          J'ai en premier lieu essayé de faire marcher le robot en faisant faire à deux pattes opposées un triangle dans la même direction. Ce mode est implémenté dans le fichier Quadruped_save.py . Cette marche n'était pas concluante le robot perdait facilement l'équilibre.
          La deuxième implémentation décrit un cycle de 4 positions et chaque patte prend tour à tour une position dans ce cycle.
          Cette implémentation était je pense une bonne idée dans le sens ou elle se rapproche un peu de la marche humaine à 4 pattes cependant, elle necessitait, je pense quelque réglage supplémentaire pour devenir réellement optimale et au vue de la difficulté algorithmique je n'ai pas osé aller plus loin dans l'implémentation. ( visible dans Quadruped_save2.py )
          Enfin l'implémentation que j'ai décidé de garder et celle où chaque bout de patte décrit un carré. Les pattes bougent par deux ( les deux pattes opposées bougent en même temps )
          J'estime que c'est celle qui est la plus optimale cependant je pense aussi qu'il reste quelques réglage à faire au niveau du timing. Si je devais réaliser ces reglages je procederai à tâtons afin de trouver le réglage optimal.  

goto :    
          Ce mode permet étant donné une position et une orientation, permet de faire déplacer le robot vers la position donnée et en étant orientée correctement.
          Il repose sur 3 valeurs que j'ai moi même recueilli en chronometrant le robot sur des distance connue afin d'en déduire les vitesse réelle suivant x,y,theta respectivement v_real_x,v_real_y,v_real_theta. Le robot ce déplace donc à 0.025 m/s selon x et y et 5.6 rad/s en rotation par rapport à l'axe z.
          Grâce à ces valeurs je peux déterminer la durée totale du mouvement et ainsi déplacé le robot à l'aide du mode walk pendant le temps voulu afin que le robot aille à la position souhaitée.
          La fonction walk supportant mal la composition des vitesse j'ai simplement décomposé le mouvement en faisant d'abord le mouvement suivant x puis suivant y et enfin en rotation suivant l'axe z.
fun :      
          Vous pourrez admirez un robot capable de réaliser un ange de neige de façon tout à fait automatique

Plus de l'implémentation :
          Le robot utilise, pour tout déplacement, la fonction interpolate qui permet de lisser la courbe de position pour et ainsi éviter que celui-ci ne fasse de mouvement brusque ce qui pourrait, dans la réalité, l'abimer.

          Le robot se place dans la position stable au tout début de la simulation, cela lui prend les deux première seconde de la simulation.

          La fonction autocollision. Assure, à tout moment que les articulation ne dépassent pas certaines valeurs angulaire cela afin d'éviter les autocollision évidentes.
