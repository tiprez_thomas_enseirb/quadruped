import argparse
import math
import pybullet as p
from time import sleep

dt = 0.01


def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF("C:\\Users\\thoma\\Documents\\Robotique\\quadruped-master\\plane.urdf")

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("C:\\Users\\thoma\\Documents\\Robotique\\quadruped-master\\quadruped\\robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot
def stable(joints):
    """
    Return : A list of hardcoded value where the robot is stable
    """
    modify_leg_id(0,inverse(10,0,-5),joints)
    modify_leg_id(1,inverse(10,0,-5),joints)
    modify_leg_id(2,inverse(10,0,-5),joints)
    modify_leg_id(3,inverse(10,0,-5),joints)
    return joints


    return joints
def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot

    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def modify_leg_id(id,val,joints,t=0):
    """
    Arguments:
        id in [0;3] représente l'id de la patte à modifier
        val  -- les 3 positions en radian à donné au moteurs
        joints -- tableau des 12 positions en radian des différents moteurs
        t -- rotation supplémentaire suivant l'axe z
    return
        le tableau joints modifié
    """
    joints[id*3]=val[0]-t
    joints[id*3+1]=val[1]
    joints[id*3+2]=val[2]
    return joints


def inverse(x,y,z):
        T=[]
        l1=4.5
        l2=6.5
        l3=8.7
        t0=math.atan2(y,x)
        T.append(t0)
        d=math.sqrt((x-l1*math.cos(t0))**2+(y-l1*math.sin(t0))**2+z**2)
        L=[]
        L.append((l2**2+d**2-l3**2)/(2*d*l2))
        L.append(z/d)
        L.append((l2**2+l3**2-d**2)/(2*l2*l3))
        for i in range (len(L)) :
            if L[i] < -1 :
                L[i]=-1
            if L[i] > 1 :
                L[i] = 1
        T.append(math.acos(L[0])+math.asin(L[1]))
        T.append((-math.acos(L[2]))-math.pi)
        return T


def leg_ik(x,y,z,joints,debug = False):
    """
    Arguments:
        t {float} -- Angle de rotation par rapport à z
        x,y,z {float} -- Coordonnées cible du bout de la patte
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
    """
    # standardization of instruction to respect the integrity of the robot
    if ( not(debug) ):
        if ( x > 17.5 ):
            x = 17.5
        if ( x < 0.25 ):
            x = 0.25
        if ( y > 11 ):
            y = 11
        if ( y < -11 ) :
            y = -11
        if ( z > 13 ):
            z = 13
        if ( z < -10 ):
            z = -10


    l1,l2,l3,l4=40,45,65,87
    joints = [0]*12
    joints=stable(joints)
    joints = modify_leg_id(2,inverse(x,y,z),joints)
    return joints

def rotation_translation(x,y,z,alpha):
    l1,l2,l3,l4=40,45,65,87
    dz = -z
    dy = y*math.cos(alpha)-x*math.sin(alpha)
    dx = y*math.sin(alpha)+x*math.cos(alpha)+l1
    return [dx,dy,dz]


def robot_ik(t,x,y,z):
    """
    Arguments:
        t {float} -- Angle de rotation du centre de gravité
        x,y,z {float} -- Coordonnées cible du centre du robot
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
    """
    #print(t)
    joints=[0]*12
    joints=stable(joints)
    for id in [0,1,2,3]:
        alpha = (2*id+1)*math.pi/4
        delta = rotation_translation(x,y,z,alpha)
        joints=modify_leg_id(id,inverse(delta[0],delta[1],delta[2]),joints,t)
    return joints



def fun(t):
    # t = math.fmod(t,4.0)
    # if (t < 1):
    #     return( robot_ik(0,-40,0,20))
    # elif (t <2):
    #     return( robot_ik(0,-40,0,2))
    # elif (t <3):
    #     return( robot_ik(0,40,0,2))
    # else :
    #     return( robot_ik(0,40,0,20))
    amplitude = 0.3
    joints = [0] * 12
    joints = modify_leg_id(1,[conv_deg_rad(30),0,0],joints)
    joints = modify_leg_id(2,[conv_deg_rad(-30),0,0],joints)
    joints = modify_leg_id(3,[conv_deg_rad(-60) + math.sin(t) * amplitude,0,0],joints)
    joints = modify_leg_id(0,[conv_deg_rad(60) - math.sin(t) * amplitude,0,0],joints)
    return joints

def walk(t_speed,x_speed,y_speed,t):
    """
    Arguments:
        t_speed en rad/s
        x_speed,y_speed en m/s

    Returns:
        list-- les 12 positions suivantes pour les moteurs.
    """
    joints = [0]*12
    joints = stable(joints)
    x_speed = x_speed * 100
    y_speed = y_speed * 100
    t_speed = t_speed * 100

    # This test prevent from moving the leg if there is no recipient
    if (x_speed == 0):
        if(y_speed == 0):
            if(t_speed ==0):
                return joints
    # ID_LIST allow us to move the different legs sequentially depending on the parity of t
    ID_LIST = []
    if (math.fmod(t,4.0)<2.0):
        ID_LIST+=[0,2]
    else:
        ID_LIST+=[1,3]

    for id in ID_LIST:
        dtheta = (2*id+1)*math.pi/4
        T=[]
        # T is the list of the position to reach for each leg
        T.append([10,0,-4])
        T.append([10,0,-5])
        T.append(position_to_reach(x_speed,-y_speed,-5,dtheta,t_speed))
        T.append([10,0,-4])
        t=math.fmod(t,2.0)

        # Data processing
        X=[P[0] for P in T]
        Y=[P[1] for P in T]
        Z=[P[2] for P in T]

        # Interpolation
        TPS=[0,0.5,1,1.5]
        dx=interpolate(TPS,X,t)
        dy=interpolate(TPS,Y,t)
        dz=interpolate(TPS,Z,t)

        # Move the leg
        joints = modify_leg_id(id,inverse(dx,dy,dz),joints)
    return joints

def position_to_reach(x,y,z,theta,theta_extra=0,default_x=10,default_y=0,default_z=-5):
    T =[default_x*math.cos(theta_extra) +default_y*math.sin(theta_extra) - x*math.cos(theta) - y*math.sin(theta),default_y*math.cos(theta_extra) - default_x*math.sin(theta_extra)
    - y*math.cos(theta) + x*math.sin(theta),z]
    return T

def interpolate(xs, ys, x):
    if x<=xs[0]:
        return ys[0];
    if x>=xs[-1]:
        return ys[-1];
    for i in range(len(xs)):
        if x<xs[i]:
            return ((ys[i]-ys[i-1])/(xs[i]-xs[i-1]))*(x-xs[i])+ys[i]

def goto(x,y,t,time,vx_max=0.5,vy_max=1,vtheta_max=0.15):
    #Data from the timing of the robot over known distances
    v_real_x,v_real_y,v_real_theta = 0.024,0.024,0.18 #respectively in m/s, m/s et rad/s
    time_needed_x,time_needed_y,time_needed_theta = x/v_real_x,y / v_real_y,t/v_real_theta
    if (time < 2 + time_needed_x):
        return(walk(0,vx_max,0,time))
    elif (time < 2 + time_needed_x + time_needed_y):
        return walk(0,0,vy_max,time)
    elif (time < 2 + time_needed_x + time_needed_y+time_needed_theta):
        return walk(vtheta_max,0,0,time)
    else :
        return( stable([0]*12))

def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[11] = math.sin(t) * amplitude
    return joints

def conv_deg_rad(deg):
    return(deg*math.pi/180)

def autocollision(joints):
    """ We define a saturation value for each motor this value must not be exceeded
    """

    saturation_mot_2_3 = conv_deg_rad(320)
    for id in [0,1,2,3]:
        if (abs(joints[id*3]) > math.pi/3):
            if(joints[id*3]<0):
                joints[id*3] = - math.pi/3

            else:
                joints[id*3] = math.pi/3


        if (abs(joints[id*3+1]) > saturation_mot_2_3):
            if(joints[id*3+1]<0):
                joints[id*3+1] = - saturation_mot_2_3

            else:
                joints[id*3+1] = saturation_mot_2_3


        if (abs(joints[id*3+2]) > saturation_mot_2_3):
            if(joints[id*3+2]<0):
                joints[id*3+2] = - saturation_mot_2_3

            else:
                joints[id*3+2] = saturation_mot_2_3

    return(joints)
if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=10.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-z', type=float, help='Z target for goto (m)', default=-5.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, theta = args.m, args.x, args.y, args.t

    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif mode =='leg_ik':
        x = p.addUserDebugParameter("x", -20,20, 10)
        y = p.addUserDebugParameter("y", -20,20, 0)
        z = p.addUserDebugParameter("z", -20,20, -5)
        print('Mode '+mode+'...')
    elif mode == 'robot_ik':
        x = p.addUserDebugParameter("x", -40,40, 0)
        y = p.addUserDebugParameter("y", -20,20, 0)
        z = p.addUserDebugParameter("z", -20,20, 25)
        theta = p.addUserDebugParameter("theta", -1,1,0.1)
        print('Mode '+mode+'...')
    elif mode == 'walk':
        x_speed = p.addUserDebugParameter("x_speed",-1,1,0)
        y_speed = p.addUserDebugParameter("y_speed",-1,1,0)
        theta_speed = p.addUserDebugParameter("theta_speed",-1,1,0)
        print('Mode walk ...')
    elif mode == 'goto':
        print('Mode goto ...')
    elif mode == 'fun':
        print('Mode fun ...')
    else:
        raise Exception('Mode non implémenté: %s' % mode)

    t = 0


    # Boucle principale
    while True:
        t += dt
        if t < 2:
            joints = [0]*12
            if mode != 'fun':
                X = [0,10]
                Y = [0,0]
                Z = [0,-5]
                # Interpolation
                TPS=[0,1.8]
                dx=interpolate(TPS,X,t)
                dy=interpolate(TPS,Y,t)
                dz=interpolate(TPS,Z,t)

                # Move the leg
                for id in [0,1,2,3]:
                    joints = modify_leg_id(id,inverse(dx,dy,dz),joints)
        else :
            if mode == 'demo':
                # Récupération des positions cibles
                joints = demo(t, p.readUserDebugParameter(amplitude))
            elif mode == 'leg_ik':
                X=[10,p.readUserDebugParameter(x)]
                Y=[0,p.readUserDebugParameter(y)]
                Z=[-5,p.readUserDebugParameter(z)]
                T=[0.0,t+0.5]
                dx=interpolate(T,X,t)
                dy=interpolate(T,Y,t)
                dz=interpolate(T,Z,t)
                joints = leg_ik(dx,dy,dz,joints)
            elif mode == 'robot_ik':
                X=[0,p.readUserDebugParameter(x)]
                Y=[0,p.readUserDebugParameter(y)]
                Z=[25,p.readUserDebugParameter(z)]
                Th=[0,p.readUserDebugParameter(theta)]
                T=[0.0,t+1]
                dx=interpolate(T,X,t)
                dy=interpolate(T,Y,t)
                dz=interpolate(T,Z,t)
                dth=interpolate(T,Th,t)
                joints =  robot_ik(dth,dx,dy,dz)
            elif mode == 'walk':
                joints =  walk(p.readUserDebugParameter(theta_speed),p.readUserDebugParameter(x_speed),p.readUserDebugParameter(y_speed),t)

            elif mode == 'goto':
                joints = goto(x,y,theta,t)
            elif mode == 'fun':
                joints = fun(t)
        if mode != 'fun':
            joints = autocollision(joints)
        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)
        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
